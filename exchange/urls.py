from django.contrib import admin
from django.urls import path
from exchange.views import PingView

urlpatterns = [
    path('ping/', PingView.as_view()),
]
