from django.shortcuts import render

# Create your views here.
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView


class PingView(APIView):
    @staticmethod
    def get(request):
        return Response("Pong", status=status.HTTP_200_OK)